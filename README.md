# Open Hardware literature review

To contribute, edit .tex file and send pull requests, keeping your editor 
configured to max-width of 80 columns. If you have a 'papeeria' account, 
you can edit and leave comments directly on the [collaborative editor](https://
www.papeeria.com/join?token_id=38d95771-de6e-4fc1-a693-e52a2703ca62&retry=3).
Please ask us for a token to obtain write permissions.

The [collaborative biblio](https://www.zotero.org/groups/2312397/open_hardware) 
is being gathered with Zotero, feel free to join.

Happy editing!
